package com.alibaba.dubbo.spring4;

import com.alibaba.citrus.webx.WebxComponents;
import com.alibaba.citrus.webx.context.WebxApplicationContext;
import com.alibaba.citrus.webx.context.WebxComponentContext;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import static com.alibaba.citrus.util.Assert.ExceptionType.ILLEGAL_STATE;
import static com.alibaba.citrus.util.Assert.assertNotNull;

/**
 * 用来初始化<code>WebxComponents</code>。
 * @author Taomk 修改支持Spring4
 */
public class WebxComponentsContext extends WebxApplicationContext {
    private WebxContextLoaderListener componentsLoader;

    public WebxContextLoaderListener getLoader() {
        return assertNotNull(componentsLoader, ILLEGAL_STATE, "no WebxComponentsLoader set");
    }

    public void setLoader(WebxContextLoaderListener loader) {
        this.componentsLoader = loader;
    }

    /**
     * 取得所有的components。
     */
    public WebxComponents getWebxComponents() {
        return getLoader().getWebxComponents();
    }

    @Override
    protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        super.postProcessBeanFactory(beanFactory);
        getLoader().postProcessBeanFactory(beanFactory);
    }

    @Override
    protected void finishRefresh() {
        super.finishRefresh();
        getLoader().finishRefresh();
    }

    /**
     * 在创建子容器时，给parent一个设置子context的机会。
     */
    protected void setupComponentContext(WebxComponentContext componentContext) {
    }
}
